# php-lock

## 说明

#### 介绍
使用多种方式实现的简易、可靠的分布式锁。

#### 架构
redis引擎基于predis/predis:"^2.0"包

#### 思想
* 使用场景：防重复提交、唯一任务按序执行、悲观锁业务。


## 使用

#### 安装

1.  composer require bdhert/php-lock
2.  开发版：composer require bdhert/php-lock:"dev-master"
3.  基础版：composer require bdhert/php-lock:"^0.1"

#### 使用

```php

<?php
use bdhert\PhpLock\Lock;
use bdhert\PhpLock\exception\LockException;
use Predis\Client;

try {
    $key   = 'my:unique';
    $redis = new Client([
        'scheme'   => 'tcp',
        'select'   => 0,
        'port'     => 6379,
        'host'     => '127.0.0.1',
        'password' => NULL,
    ]);

    $lock = new Lock(Lock::LOCK_TYPE_REDIS, $redis);
    if ($lock->get($key)) {
        echo 'OK';
    }

    $lock->release($key);
} catch (LockException $e) {
    throw new LockException($e->getMessage(), 400);
}

```

