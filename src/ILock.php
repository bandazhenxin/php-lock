<?php
namespace bdhert\PhpLock;

interface ILock{
    /**
     * 根据具体的并发情况配置以下参数
     * EXPIRE 取锁超时时间 秒 越小响应越即时、唯一性越好，越大安全性越高。为0的时候不一定能防止重复数据，因为一组连续请求即时拿锁会导致未经过时间差补足，而时间差补足逻辑不能在初次获取时触发，那样会大大影响性能
     * LOCK_WAIT 锁定时间 毫秒 超过程序执行的可能阻塞时长
     * LOCK_SLEEP 取锁间隔 微秒
     * LOCK_DIFF 时间差补足 微秒 如补足主从延迟(查询历史延迟峰值)。 目前猜测完整的延迟时间时主从延迟的时间和 越大准确性越好 这里也有漏洞，正好第一次拿到锁的都不会主动延迟，要是都主动延迟会大大降低性能
     *
     * 以上可以做一个数学模型，完全可以定量分析
     */
    const EXPIRE     = 15;
    const LOCK_WAIT  = 50000;
    const LOCK_SLEEP = 10000;
    const LOCK_DIFF  = 0;

    public function get($key, $timeout = self::EXPIRE);
    public function release($key);
}
